package com.example.bookapp.model

data class Book(
    val id: String,
    val name: String,
    val author: String,
    val genre: String,
    val imgUrl: String
)