package com.example.bookapp.data.network

import retrofit2.http.GET

interface BookApi {

    @GET("_Crime&Th?node=1318161031&ie=UTF8")
    fun getCrime()

}