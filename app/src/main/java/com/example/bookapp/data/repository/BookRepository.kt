package com.example.bookapp.data.repository

import com.example.bookapp.model.Book
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BookRepository @Inject constructor(private val bookLab: BookLab) {

    fun getBooks(type: String): List<Book> {
        when {
            type.equals("sci-fi") -> return bookLab.sciFiList
            type.equals("action") -> return bookLab.actionList
            type.equals("education") -> return bookLab.educationList
            type.equals("thriller") -> return bookLab.thrillerList
            type.equals("romance") -> return bookLab.romanceList
            else -> return throw IllegalArgumentException("No Books Found")
        }
    }
}