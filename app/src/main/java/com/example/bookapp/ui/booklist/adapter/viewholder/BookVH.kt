package com.example.bookapp.ui.booklist.adapter.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bookapp.R
import com.example.bookapp.model.Book
import kotlinx.android.synthetic.main.item_book.view.*

class BookVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mCoverIV = itemView.findViewById<ImageView>(R.id.coverIV)
    private val mName = itemView.findViewById<TextView>(R.id.nameTV)
    private val mGenre = itemView.findViewById<TextView>(R.id.genreIV)
    private val mId = itemView.findViewById<TextView>(R.id.idTV)
    private val mAuthor = itemView.findViewById<TextView>(R.id.authorTV)

    fun bindBook(book: Book) {
        mName.text = book.name
        mGenre.text = book.genre
        mId.text = book.id
        mAuthor.text = book.author
    }
}