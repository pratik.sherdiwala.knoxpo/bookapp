package com.example.bookapp.ui.booklist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookapp.data.repository.BookRepository
import com.example.bookapp.model.Book
import javax.inject.Inject

class BookListViewModel @Inject constructor(private val bookRepository: BookRepository) : ViewModel() {

    val itemList = MutableLiveData<List<Book>>()

    fun getBooks(type: String) {
        itemList.value = bookRepository.getBooks(type)
    }
}