package com.example.bookapp.ui.common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.bookapp.model.Book
import com.example.bookapp.ui.booklist.adapter.BookAdapter

object DataBindingInjector {

    @JvmStatic
    @BindingAdapter("items")
    fun setBook(view: RecyclerView, items: List<Book>?) {
        items?.let {
            (view.adapter as? BookAdapter)?.updateBook(it)
        }
    }
}