package com.example.bookapp.ui.tab

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.bookapp.databinding.ActivityMainBinding
import com.example.bookapp.ui.booklist.BookListFragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil
            .setContentView<ActivityMainBinding>(
                this,
                com.example.bookapp.R.layout.activity_main
            )


        val activity = this

        with(binding) {
            lifecycleOwner = activity

            setUpViewPager(viewPager)

            tabLayout.setupWithViewPager(viewPager)
        }
    }

    private fun setUpViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)

        adapter.addFragment(
            BookListFragment.newInstance("romance"),
            "Romance"
        )
        adapter.addFragment(
            BookListFragment.newInstance("thriller"),
            "Thriller"
        )
        adapter.addFragment(
            BookListFragment.newInstance("education"),
            "Education"
        )
        adapter.addFragment(
            BookListFragment.newInstance("action"),
            "Action"
        )

        adapter.addFragment(
            BookListFragment.newInstance("sci-fi"),
            "Sci-Fi"
        )
        viewPager.adapter = adapter
    }


}