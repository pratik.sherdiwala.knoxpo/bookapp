package com.example.bookapp.ui.tab

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val fragments = ArrayList<Fragment>()
    private val fragmentTitles = ArrayList<String>()

    fun addFragment(fm: Fragment, title: String) {
        fragments.add(fm)
        fragmentTitles.add(title)
    }


    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }


    override fun getCount() = fragments.size

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentTitles[position]
    }

}