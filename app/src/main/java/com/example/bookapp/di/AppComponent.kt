package com.example.bookapp.di

import com.example.bookapp.App
import com.example.bookapp.di.module.NetworkModule
import com.example.bookapp.di.module.ViewModelModule
import com.example.bookapp.di.module.ui.MainActivityModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        MainActivityModule::class,
        ViewModelModule::class,
        NetworkModule::class
    ]
)

interface AppComponent {

    fun inject(app: App)

}