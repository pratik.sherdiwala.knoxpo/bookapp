package com.example.bookapp.di.module

import com.example.bookapp.data.network.BookApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://www.amazon.in/b/ref=gbpp_itr_m-3_a4fa")
            .build()
    }

    @Provides
    fun provideBookApi(retrofit: Retrofit):BookApi{
        return retrofit.create(BookApi::class.java)
    }

}