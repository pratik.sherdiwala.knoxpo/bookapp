package com.example.bookapp.di.module.ui

import com.example.bookapp.ui.booklist.BookListFragment
import com.example.bookapp.ui.tab.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = [MainActivityFragmentModule::class])
    abstract fun contributeMainActivity(): MainActivity

}

@Module
abstract class MainActivityFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeBookListFragment(): BookListFragment
}